package com.oitic.takephotoandsend;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class MainActivity extends AppCompatActivity {

    String currentImagePath = null;
    private static final int IMAGE_REQUEST = 1;
    private String[] permissions = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.ACCESS_FINE_LOCATION", "android.permission.READ_PHONE_STATE", "android.permission.SYSTEM_ALERT_WINDOW", "android.permission.CAMERA"};

    Button btnTakeaPhoto;
    Button btnSendImage;

    ImageView imageViewPhoto;

    byte[] byteArrayImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnTakeaPhoto = findViewById(R.id.btnTakePhoto);
        btnSendImage = findViewById(R.id.btnSubirImage);

        imageViewPhoto = findViewById(R.id.imgPicture);

        //Permisos de Acceso a camara y guardar imagen
        int requestCode = 200;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }


        btnTakeaPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturarImage();
            }
        });
        btnSendImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    enviarImagePost();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        btnSendImage.setEnabled(false);
    }


    public void capturarImage() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File imageFile = null;
            try {
                imageFile = getImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (imageFile != null) {
                try {
                    Uri imageUri = FileProvider.getUriForFile(this,
                            BuildConfig.APPLICATION_ID + ".provider", //.provider en manifest y en xml paths para la ruta
                            imageFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

                    startActivityForResult(cameraIntent, IMAGE_REQUEST);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //Crea el Archivo File
    private File getImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageName = "oitic-image" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        File imageFile = File.createTempFile(imageName, ".jpg", storageDir);

        currentImagePath = imageFile.getAbsolutePath();

        return imageFile;

    }

    public void enviarImagePost() throws JSONException {

        try {
            final OkHttpClient client = new OkHttpClient();

            JSONObject jsonResult = new JSONObject();
            jsonResult.put("nombre", "Bryan");
            jsonResult.put("idMymeType", "jpeg");
            jsonResult.put("idTipoImagen", 1);
            jsonResult.put("idOrigenImagen", 1);
            jsonResult.put("idSistema", "mercadeo");
            final String baseUrl = "http://www.grancompu.com/servicio/imagen/enviar";

// Use the imgur image upload API as documented at https://api.imgur.com/endpoints/image
            Map<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/form-data");
            MediaType mediaType = MediaType.parse(""); // set the media type to empty since the headers will have the needed the media type.
            Headers headerbuild = Headers.of(headers);

//new File("/home/pc5/Documentos/oitic_logo.png")
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("imageInfo", jsonResult.toString())
                    .addFormDataPart("uploadfile", "photo.jpeg",
                            RequestBody.create(mediaType, byteArrayImage))
                    .build();

            Request request = new Request.Builder()
                    .headers(headerbuild)
                    .url(baseUrl)
                    .post(requestBody)
                    .build();
            try {
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        String mMessage = e.getMessage().toString();
                        Log.w("failure Response", mMessage);
                    }

                    @Override
                    public void onResponse(Call call, final Response response) throws IOException {

                        final String mMessage = response.body().string();
                        Log.e(TAG, mMessage);
                        Log.e(TAG, response.toString());

                        try {
                            //hilo  para mostrar toast dentro de la response
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Imagen Subida Exitosamente" + mMessage, Toast.LENGTH_LONG).show();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == IMAGE_REQUEST && resultCode != 0 ){
            Bitmap bitmap = BitmapFactory.decodeFile(currentImagePath);
            bitmap = RotateBitmap(bitmap, 90);
            imageViewPhoto.setImageBitmap(bitmap);


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            byteArrayImage = stream.toByteArray();
            btnSendImage.setEnabled(true);
        }


    }

    //Funcion rotación de imagen
    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
}
